﻿using System;
using System.Threading;

namespace Consoleapplicatie_Werkcolleges_CSharp
{
    class Program

    {

        static void Main(string[] args)

        {

            int i = 1;

            for (i = 0; i < 10; i++)
            {
                Console.CursorLeft = 0;
                Console.CursorTop = 0;
                Console.WriteLine("I: " + i++);
                Console.WriteLine("I: " + i++);
                Thread.Sleep(300);
            }
        }
    }
}
